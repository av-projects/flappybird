function CircleCollider(parent, radius) {
	Collider.call(this, parent, radius, radius);

	this.overlaps = function(obj) {
		if(obj && obj.collider) {

		}

		return false;
	};
}

CircleCollider.prototype.rectCollision = function(rectCollider) {
	if(!rectCollider instanceof RectCollider) {
		return false;
	}

	var distX = Math.abs(this.getX() - rectCollider.getX()),
		distY = Math.abs(this.getY() - rectCollider.getY());

	if(distX > (rectCollider.getW()*.5 + this.getW())) { return false; }
	if(distY > (rectCollider.getH()*.5 + this.getH())) { return false; }

	if(distX <= rectCollider.getW()*.5) { return true; }
	if(distY <= rectCollider.getH()*.5) { return true; }

	var corner = Math.pow(distX - rectCollider.getW()*.5, 2) + Math.pow(distY - rectCollider.getH()*.5, 2);

	return (corner <= Math.pow(this.getW(), 2));
};

CircleCollider.prototype.draw = function(ctx) {
	var parent = this.getParent();

	var x = parent.getX(), y = parent.getY(),
		r = this.getW();

	ctx.strokeStyle = "red";
	ctx.beginPath();
	ctx.arc(x, y, r, 0, 2*Math.PI);
	ctx.closePath();
	ctx.stroke();
};