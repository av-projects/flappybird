function RectCollider(parent, w, h) {
	Collider.call(this, parent, w, h);

	this.overlaps = function(obj) {
		if(!obj instanceof Collider) {
			return false;
		}

		if(
			RectCollider.prototype.circleCollision.call(this, obj) || 
			RectCollider.prototype.rectCollision.call(this, obj)
		) { return true; }

		return false;
	};
}

RectCollider.prototype.rectCollision = function(rectCollider) {
	if(!(rectCollider instanceof RectCollider)) {
		return false;
	}

	return true;
};

RectCollider.prototype.circleCollision = function(circleCollider) {
	if(!(circleCollider instanceof CircleCollider)) {
		return false;
	}

	var pCircle = circleCollider.getParent(),
		pRect = this.getParent();

	var distX = Math.abs(pCircle.getX() - pRect.getX()),
		distY = Math.abs(pCircle.getY() - pRect.getY());

	if(distX > (this.getW()*.5 + circleCollider.getW())) { return false; }
	if(distY > (this.getH()*.5 + circleCollider.getH())) { return false; }

	if(distX <= this.getW()*.5) { return true; }
	if(distY <= this.getH()*.5) { return true; }

	var corner = Math.pow(distX - this.getW()*.5, 2) + Math.pow(distY - this.getH()*.5, 2);

	return (corner <= Math.pow(pCircle.getW(), 2));
};

RectCollider.prototype.draw = function(ctx) {
	var parent = this.getParent();

	var x = parent.getX(), y = parent.getY(),
		w = this.getW(), h = this.getH();

	ctx.strokeStyle = "red";
	ctx.beginPath();
	ctx.moveTo(x-w*.5, y-h*.5);
	ctx.lineTo(x-w*.5+w, y-h*.5);
	ctx.lineTo(x-w*.5+w, y-h*.5+h);
	ctx.lineTo(x-w*.5, y-h*.5+h);
	ctx.lineTo(x-w*.5, y-h*.5);
	ctx.closePath();
	ctx.stroke();
};