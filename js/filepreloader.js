function FilePreloader() {
	this.FILE_TYPE = {
		ArrayBuffer: "arraybuffer",
		Blob: "blob",
		Document: "document",
		JSON: "json",
		Text: "text"
	};

	var mFiles = [],
		mLoadCount = 0,
		mTotalFiles = 0;

	this.getFiles = function() { return mFiles; };
	this.getTotalFiles = function() { return mTotalFiles; };

	this.getLoadCount = function() { return mLoadCount; };
	this.setLoadCount = function() { mLoadCount++; };

	/**
	 * Loads files over AJAX
	 * @param  mixed 		src      Array of file paths ([path, [path, FILE_TYPE], ...])
	 * @param  function  	callback Returns the data in the order of the src array
	 */
	this.load = function(src, callback) {
		mTotalFiles = src.length;

		var file,
			path = 0, type = 1;
		for(var i in src) {
			file = src[i];

			if(file instanceof Array) {
				this.getFile(file[path], i, callback, file[type]);
				continue;
			}

			this.getFile(file, i, callback);
		}
	};

	this.pushFile = function(file, index) { mFiles[index] = file; };
	/**
	 * Ajax request
	 * @param  string   file     File path
	 * @param  int   	index    File index
	 * @param  function callback Final callback
	 */
	this.getFile = function(file, index, callback, type) {
		var xhr = new XMLHttpRequest();
		xhr.open('get', file);

		if(type) { xhr.responseType = type }

		xhr.onreadystatechange = FilePreloader.prototype.getResponse.bind(this, xhr, index);
		xhr.onloadend = FilePreloader.prototype.ready.bind(this, callback);
		xhr.send();
	};
}

/**
 * Ajax response
 * @param  XMLHttpRequest 	xhr   Native JS Ajax object
 * @param  int 				index File index
 */
FilePreloader.prototype.getResponse = function(xhr, index) {
	if(xhr.readyState != XMLHttpRequest.DONE) { return; }

	if(xhr.status >= 200 && xhr.status < 300) {
		this.setLoadCount();

		if(xhr.responseType == '' || xhr.responseType == this.FILE_TYPE.Text) {
			this.pushFile(xhr.responseText, index);
			return;
		}

		this.pushFile(xhr.response, index);
		return;
	}

	throw "Failed ajax call with status: " + xhr.status;
};

/**
 * Checks the file counter
 * @param  function callback Executes if all files are loaded
 */
FilePreloader.prototype.ready = function(callback) {
	if(this.getLoadCount() < this.getTotalFiles()) { return; }

	var self = this;
	setTimeout(function(){
		callback.apply(self, self.getFiles());
	},500);
};