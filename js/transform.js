function Transform(x, y, w, h) {
	var mX = x || 0, mY = y || 0,
		mW = w || 0, mH = h || 0;

	this.getX = function() { return mX; };
	this.setX = function(val) { mX = val; };

	this.getY = function() { return mY; };
	this.setY = function(val) { mY = val; };

	this.getW = function() { return mW; };
	this.setW = function(val) { mW = val; };

	this.getH = function() { return mH; };
	this.setH = function(val) { mH = val; };
}