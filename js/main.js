var fps = document.getElementById('fps'),
	counter = new FPSCounter();

var preloader = new FilePreloader();
preloader.load([['game.json', preloader.FILE_TYPE.JSON]], startGame);

var score = 0,
	scoreBoard = document.getElementById('scoreboard'),
	highscore = document.getElementById('highscore');

var ctx, running = false;
function initialize(game) {
	if(score > parseInt(highscore.innerText))
	highscore.innerText = score;
	score = 0;
	scoreBoard.innerText = score;

	if(running) { return; }
	running = true;

	var wrapper = document.getElementById('flappy-bird');
	var canvas = document.createElement('canvas');
	canvas.width = game.width;
	canvas.height = game.height;
	wrapper.appendChild(canvas);

	ctx = canvas.getContext('2d');
}

function startGame(game, test) {
	initialize(game);

	var player = game.player;
	var scene = {
		settings: game,
		obstacles: [],
		player: new Player(player.x, game.height*.5, player.size, "yellow")
	};

	requestAnimationFrame(loop.bind(this, ctx, scene));
}

function createObstaclePair(obstacles, obstacle, game) {
	var x = game.width+obstacle.width,
		y1 = rand(obstacle.minY, obstacle.maxY),
		y2 = y1 + obstacle.spacing.x,
		h1 = y1*2, h2 = (game.height - y2)*2;

	obstacles.push(new Obstacle(x, y1, obstacle.width, h1));
	obstacles.push(new Obstacle(x, y2, obstacle.width, h2));
}

function rand(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function loop(ctx, scene) {
	fps.innerText = counter.count() + " fps";

	repaint(ctx, scene);
	if(!applyLogics(scene)) {
		startGame(scene.settings);
		return;
	}

	requestAnimationFrame(loop.bind(this, ctx, scene));
}

function repaint(ctx, scene) {
	var obstacles = scene.obstacles,
		game = scene.settings,
		player = scene.player;
	ctx.clearRect(0,0,game.width,game.height);

	var el;
	for(var i in obstacles) {
		el = obstacles[i];

		el.draw(ctx);
		drawCollider(el, ctx);
	}

	player.draw(ctx);
	drawCollider(player, ctx);
}

function drawCollider(element, ctx) {
	if(element.drawCollider) {
		element.drawCollider(ctx);
	}
}

function applyLogics(scene) {
	var game = scene.settings,
		obstacles = scene.obstacles,
		obstacle = scene.settings.obstacle,
		player = scene.player;

	var deltaTime = counter.getDeltaTime(game.fps),
		obstacleSpeed = obstacle.speed * deltaTime;

	player.update(deltaTime);

	var el, shiftObstacle = false;
	for(var i in obstacles) {
		el = obstacles[i];

		if(el.getX() < -el.getW()) {
			shiftObstacle = true;
		}

		el.setX(el.getX()-obstacleSpeed);

		// obstacle collision
		if(el.collider.overlaps(player.collider)) {
			return false;
		}

		// obstacle pass
		if(el.isPassed(player)) {
			score += 0.5;
			scoreBoard.innerText = score;
		}
	}

	// ground collision
	if(player.getY()+player.collider.getH()*.5 > game.height || player.getY()-player.collider.getH()*.5 < 0) {
		return false;
	}

	if(shiftObstacle) {
		obstacles.shift();
		obstacles.shift();
	}

	var lastElement = obstacles[obstacles.length-1];
	if(!lastElement || (!!lastElement && lastElement.getX() < game.width-obstacle.spacing.y)) {
		createObstaclePair(obstacles, obstacle, game);
	}

	return true;
}