function FPSCounter() {
	var mFilterStrength = 20,
		mFrameTime = 0, mLastLoop = new Date, mCurrentLoop;

	this.count = function() {
		var currentFrameTime = (mCurrentLoop=new Date) - mLastLoop;
		mFrameTime += (currentFrameTime - mFrameTime) / mFilterStrength;
		mLastLoop = mCurrentLoop;

		return 1000/mFrameTime;
	};

	this.getFrameTime = function() { return mFrameTime; };
	this.getDeltaTime = function(fps) { return mFrameTime*0.001*fps; };
}