function Obstacle(x, y, w, h, color) {
	Transform.call(this, x, y, w, h);

	this.collider = new RectCollider(this, w, h);

	var mColor = color || "white",
		mPassed = false;

	this.getColor = function() { return mColor; };
	this.setColor = function(val) { mColor = val; };

	this.isPassed = function(player) {
		if(mPassed) { return false; }

		if(player.getX() >= this.getX()) {
			mPassed = true;
		}

		return mPassed;
	};
}

Obstacle.prototype.draw = function(ctx) {
	var x = this.getX(), y = this.getY(),
		w = this.getW(), h = this.getH();

	ctx.fillStyle = this.getColor();
	ctx.beginPath();
	ctx.fillRect(x-w*.5, y-h*.5, w, h);
	ctx.closePath();
};

Obstacle.prototype.drawCollider = function(ctx) {
	this.collider.draw(ctx);
};