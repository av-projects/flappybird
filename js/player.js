function Player(x, y, size, color) {
	Transform.call(this, x, y, size, size);

	this.collider = new CircleCollider(this, size*0.8);

	var mColor = color || "yellow";

	var mVelocity = 0,
		mVelocityDecrease = 0;

	this.getColor = function() { return mColor; };
	this.setColor = function(val) { mColor = val; };

	this.getVelocity = function() { return mVelocity; };
	this.addVelocity = function() {
		mVelocity = -6;
		mVelocityDecrease = -0.006;
	};
	this.subVelocity = function(delta) {
		mVelocity += (0.2+mVelocityDecrease)*delta;
		mVelocityDecrease += 0.009*delta;
	}

	var self = this;
	document.onclick = function() { self.fly(); };
}

Player.prototype.fly = function() { this.addVelocity(); };
Player.prototype.update = function(delta) {
	this.setY(this.getY()+this.getVelocity());
	this.subVelocity(delta);
};


Player.prototype.draw = function(ctx) {
	var x = this.getX(), y = this.getY(),
		r = this.getW();

	ctx.fillStyle = this.getColor();
	ctx.beginPath();
	ctx.arc(x, y, r, 0, 2*Math.PI);
	ctx.closePath();
	ctx.fill();
};

Player.prototype.drawCollider = function(ctx) {
	this.collider.draw(ctx);
};