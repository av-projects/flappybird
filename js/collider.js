function Collider(parent, w, h) {
	Transform.call(this, parent.getX(), parent.getY(), w, h || w);

	var mParent = parent;

	this.getParent = function() { return mParent; };
	this.setParent = function(val) { mParent = val; };
}